﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Die Kennung für ein gesetztes AR Objekt. Wird im SelectionController verwendet
 */
public class PlacementObject : MonoBehaviour
{
public bool IsSelected { get; set; }
}
