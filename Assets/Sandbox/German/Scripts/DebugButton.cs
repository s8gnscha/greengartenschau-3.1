using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * wurde verwendet, um per knopfdruck objekte an und aus zu schalten
 */
public class DebugButton : MonoBehaviour
{

    
    public GameObject intro;
    
    //Mission1
    public GameObject mainQuest;
    public GameObject sideQuest;
    public GameObject flower;
    public GameObject bird;
    
    //Mission4
    public GameObject mission4;
    
    //Mission5
    public GameObject mission5;

    private bool exist;
    // Start is called before the first frame update
    void Start()
    {
        exist = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Debug()
    {
        
        if (exist)
        {
            intro.SetActive(true);
            mainQuest.SetActive(true);
            sideQuest.SetActive(true);
            flower.SetActive(true);
            bird.SetActive(true);
            //mission4.SetActive(true);
            //mission5.SetActive(true);
            exist = false;
        }
        else
        {
            intro.SetActive(false);
            mainQuest.SetActive(false);
            sideQuest.SetActive(false);
            flower.SetActive(false);
            bird.SetActive(false);
            
            //mission4.SetActive(false);
            //mission5.SetActive(false);
            exist = true;
        } 
    }
}
