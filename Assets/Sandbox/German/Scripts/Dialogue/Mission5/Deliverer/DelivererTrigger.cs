using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class DelivererTrigger : MonoBehaviour
{
	public Dialogue dialogue;
    public Dialogue dialoguepos;
    public Dialogue dialoguedis;
    public Dialogue dialogueneg;
    public Dialogue dialogueneg2;
    public Dialogue dialoguefin;
    public Dialogue dialogueloop;
    public Dialogue dialoguefinloop;
    public Dialogue dialogueintro;





    public Item[] items;
    public Dialogue newQuest;
    public GameObject leader;
	public GameObject getter;
	public GameObject getter2;
	public GameObject getter3;
    private Animator animator;

    Quests quests;
    public Quest quest;
    public GameObject answerBox1;
    public GameObject answerBox2;

    private Inventory inventory;

    
    public GameObject selectionController;
    public GameObject signature;

    bool loop;
    bool touch;
    private bool positive;

    private int missionCounter;
    
    
    // Start is called before the first frame update
    void Start()
    {
        missionCounter = 0;
        quests = Quests.instance;
        inventory = Inventory.instance;
        loop = false;
        touch = true;
        positive = false;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void TriggerDialogue()
    {
        if (leader.GetComponent<LeaderTrigger>().GetIntroDone())
        {
            if (touch)
            {
                selectionController.GetComponent<SelectionController>().SetTouch(false);
                touch = false;
                if (!positive)
                {
                    if (missionCounter < 3)
                    {
                        if (loop)
                        {
                            StartCoroutine(AudioDialogueLoop());
                        }
                        else
                        {
                            //FindObjectOfType<MainQuestManager>().StartDialogue(dialogue);
                            StartCoroutine(AudioDialogue());
                        }
                    }
                    else
                    {
                        EndDialogFinal();
                    }
                }
            }
            else
            {
                StartCoroutine(AudioDialoguePositiveLoop());
            }
        }
        else
        {
            if (touch)
            {
                selectionController.GetComponent<SelectionController>().SetTouch(false);
                touch = false;
                StartCoroutine(AudioDialogueIntro());
            }
        }
    }


    public void IncreaseMissionCounter()
    {
        missionCounter++;
    }
    public void EndDialogDiscuss1()
    {
        //FindObjectOfType<MainQuestManager2>().StartDialogue(dialoguedis);
        StartCoroutine(AudioDialogueDiscuss1());
    }
    
    public void EndDialogDiscuss2()
    {
        //FindObjectOfType<MainQuestManager2>().StartDialogue(dialoguedis);
        StartCoroutine(AudioDialogueDiscuss2());
    }
    
    public void EndDialogNegative()
    {
        //FindObjectOfType<DialogueManager>().StartDialogue(dialogueneg);
        
        quests.AddQuest(quest);
        FindObjectOfType<DialogueManager>().StartDialogue(newQuest);
        inventory.AddItem(items[0]);
        inventory.AddItem(items[1]);
        inventory.AddItem(items[2]);
		getter.GetComponent<Getter1Trigger>().SetSpeakable(false);
		getter2.GetComponent<Getter2Trigger>().SetSpeakable(false);
		getter3.GetComponent<Getter3Trigger>().SetSpeakable(false);
        StartCoroutine(AudioDialogueNegative());
    }

    public void EndDialogNegative2()
    {
        //FindObjectOfType<DialogueManager>().StartDialogue(dialogueneg2);
        
        quests.AddQuest(quest);
        FindObjectOfType<DialogueManager>().StartDialogue(newQuest);
        
        inventory.AddItem(items[0]);
        inventory.AddItem(items[1]);
        inventory.AddItem(items[2]);
		getter.GetComponent<Getter1Trigger>().SetSpeakable(false);
		getter2.GetComponent<Getter2Trigger>().SetSpeakable(false);
		getter3.GetComponent<Getter3Trigger>().SetSpeakable(false);
        StartCoroutine(AudioDialogueNegative2());
    }
    
    public void EndDialogFinal()
    {
        quest.done = true;
        quests.ChangeQuest(quest);
        //FindObjectOfType<DialogueManager>().StartDialogue(dialoguefin);
        StartCoroutine(AudioDialogueFinal());
        signature.GetComponent<SignatureCounter>().IncreaseCounter();
        positive = true;
    }
    
    IEnumerator AudioDialogue()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DelivererBeginning");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogue);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(3f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        answerBox1.SetActive(true);

    }
    
    IEnumerator AudioDialogueDiscuss1()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DelivererDiscuss1");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguepos);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(6f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        answerBox2.SetActive(true);

    }
    
    IEnumerator AudioDialogueDiscuss2()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DelivererDiscuss2");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguedis);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(6f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        answerBox2.SetActive(true);

    }
    
    IEnumerator AudioDialogueNegative()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DelivererMission1");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueneg);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(4f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(9f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(5f);
		animator.SetInteger("AnimState", 0);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
        

    }

    IEnumerator AudioDialogueNegative2()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DelivererMission2");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueneg2);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(2f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(9f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(5f);
	    animator.SetInteger("AnimState", 0);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
        
    }
    
    IEnumerator AudioDialogueLoop()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DelivererLoop");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueloop);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(3f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;

    }
    
    IEnumerator AudioDialogueIntro()
    {
        FindObjectOfType<AudioManager>().Play("DelivererIntro");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueintro);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(5f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch =true;
    }
    
    IEnumerator AudioDialogueFinal()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DelivererWin");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguefin);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(4f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();

        animator.SetInteger("AnimState", 1);
    }

    IEnumerator AudioDialoguePositiveLoop()
    {
        FindObjectOfType<AudioManager>().Play("DelivererPositiveLoop");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguefinloop);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(4f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		touch=true;
        animator.SetInteger("AnimState", 1);
    }
}
