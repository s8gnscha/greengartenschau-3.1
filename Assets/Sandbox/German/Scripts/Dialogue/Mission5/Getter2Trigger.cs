using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Verhalten und Audio des zweiten Pflegers. Läuft analog zu Mission 1 und Intro
 */
public class Getter2Trigger : MonoBehaviour
{
	public Dialogue intro;
	public Dialogue beginning;
	public Dialogue final;
	public Dialogue posloop;


    public Dialogue newQuest;
    public GameObject leader;
	public GameObject deliverer;
    public GameObject selectionController;
    Quests quests;
    public Quest quest;
    private Animator animator;


    bool loop;
    bool touch;
    bool round1;
    
    private bool speakAble;

    void Start()
    {
        speakAble = false;
        quests = Quests.instance;
        loop = false;
        touch = true;
        round1 = false;
        animator = GetComponent<Animator>();

    }

    public void TriggerDialogue()
    {
        if (leader.GetComponent<LeaderTrigger>().GetIntroDone()){
            if (touch)
            {
                selectionController.GetComponent<SelectionController>().SetTouch(false);
                touch = false;
                if (loop)
                {
                    StartCoroutine(AudioDialoguePositiveLoop());
                }
                else
                {
                    //FindObjectOfType<MainQuestManager>().StartDialogue(dialogue);
                    StartCoroutine(AudioDialogue());
                   /* if (!round1)
                    {
                        quests.AddQuest(quest);
                        FindObjectOfType<DialogueManager>().StartDialogue(newQuest);
                        round1 = true;
                    }*/

                }
            }
        }
        else
        {
            if (touch)
            {
                selectionController.GetComponent<SelectionController>().SetTouch(false);
                touch = false;
                StartCoroutine(AudioDialogueIntro());
            }
        }
        //FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
        // StartCoroutine(AudioDialogue());
    }

    IEnumerator AudioDialogue()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("Getter2Beginning");
        animator.SetInteger("AnimState", 2);
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(beginning);
        yield return new WaitForSeconds(10f);
        animator.SetInteger("AnimState", 0);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        speakAble = true;
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        

    }


    public void PositiveAnswer()
    {

        //FindObjectOfType<DialogueManager>().StartDialogue(posAnswer);
		deliverer.GetComponent<DelivererTrigger>().IncreaseMissionCounter();
        StartCoroutine(AudioDialoguePositive());
        loop = true;
    }

    IEnumerator AudioDialoguePositive()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("Getter2Final");
        animator.SetInteger("AnimState", 2);
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(final);
        yield return new WaitForSeconds(11f);
        animator.SetInteger("AnimState", 0);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;

    }
    
    IEnumerator AudioDialogueIntro()
    {
        FindObjectOfType<AudioManager>().Play("Getter2Intro");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(intro);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(5f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
    }
    
    IEnumerator AudioDialoguePositiveLoop()
    {
        FindObjectOfType<AudioManager>().Play("Getter2Thanks");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(posloop);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(2f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
    }
    
    public void SetSpeakable(bool speakAble2)
    {
        speakAble = speakAble2;
    }

    public bool GetSpeakAble()
    {
        return speakAble;
    }
}
