using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Verhalten und Audio des fünften Pflegers. Läuft analog zu Mission 1 und Intro (wird nicht verwendet)
 */
public class Getter5Trigger : MonoBehaviour
{
    public Dialogue newQuest;
    Quests quests;
    public Quest quest;
    private Animator animator;


    bool loop;
    bool touch;
    bool round1;

    void Start()
    {
        quests = Quests.instance;
        loop = false;
        touch = true;
        round1 = false;
        animator = GetComponent<Animator>();

    }

    public void TriggerDialogue()
    {

        if (touch)
        {
            touch = false;
            if (loop)
            {
                StartCoroutine(AudioDialoguePositive());
            }
            else
            { //FindObjectOfType<MainQuestManager>().StartDialogue(dialogue);
                StartCoroutine(AudioDialogue());
                if (!round1) {
                    quests.AddQuest(quest);
                    FindObjectOfType<DialogueManager>().StartDialogue(newQuest);
                    round1 = true;
                }
                
            }

        }
        //FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
        // StartCoroutine(AudioDialogue());
    }

    IEnumerator AudioDialogue()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("GetterBeginning");
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(10f);
        animator.SetInteger("AnimState", 0);
        touch = true;
        

    }


    public void PositiveAnswer()
    {

        //FindObjectOfType<DialogueManager>().StartDialogue(posAnswer);
        StartCoroutine(AudioDialoguePositive());
        loop = true;
    }

    IEnumerator AudioDialoguePositive()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("GetterThanks");
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(10f);
        animator.SetInteger("AnimState", 0);
        touch = true;
        loop = true;

    }
}
