﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/**
 * Verhalten und Audio beim Supporter. Läuft nach dem selben Prinzip wie Alter Herr und Intropersonen
 */
public class SupportTrigger : MonoBehaviour
{
    public Dialogue intro;
    public Dialogue introloop;
    public Dialogue hint;
    public Dialogue hintloop;
    public Dialogue posloop;

    private Animator animator;

    public GameObject leader;

    public GameObject mainQuest;
    public GameObject selectionController;

    bool start = false;
    bool done = false;
    

    bool loop = false;
    bool touch = true;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void TriggerDialogue()
    {
        if (leader.GetComponent<LeaderTrigger>().GetIntroDone())
        {
            
            if (touch)
            {
                selectionController.GetComponent<SelectionController>().SetTouch(false);
                touch = false;
                if (!mainQuest.GetComponent<MainQuestTrigger>().GetPositive())
                {
                    if (!start)
                    {
                        StartCoroutine(AudioDialogueStart());
                    }
                    else
                    {
                        if (loop)
                        {
                            StartCoroutine(AudioDialogueLoop());
                        }
                        else
                        {
                            //FindObjectOfType<MainQuestManager>().StartDialogue(dialogue);
                            StartCoroutine(AudioDialogue());
                        }

                    }
                }
                else
                {
                    StartCoroutine(AudioDialogueLoopPositive());
                }
                
            }
        }
        else
        {
            if (touch)
            {
                selectionController.GetComponent<SelectionController>().SetTouch(false);
                touch = false;
                StartCoroutine(AudioDialogueIntro());
            }
        }
    }


    public void SetStart(bool start)
    {
        this.start = start;

    }

    public void SetDone (bool done)
    {
        this.done = done;
    }


    IEnumerator AudioDialogue()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("SupporterHinweis");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(hint);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(7f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(9f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(8f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(6f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(6f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
    }

    IEnumerator AudioDialogueLoop()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("SupporterLoop");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(hintloop);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(3f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
    }

    IEnumerator AudioDialogueStart()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("SupporterAnfang");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(introloop);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(4f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        
    }


    IEnumerator AudioDialogueIntro()
    {
        FindObjectOfType<AudioManager>().Play("SupporterIntro");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(intro);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(5f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
    }

    IEnumerator AudioDialogueLoopPositive()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("SupporterLoopPositive");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(posloop);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(4f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        
    }

}
