﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Verhalten und Audio beim Kleinen Mädchen. Läuft nach dem selben Prinzip wie Alter Herr und Intropersonen
 */
public class SideQuestTrigger : MonoBehaviour
{
	public Dialogue intro;
    public Dialogue dialogue;
    public Dialogue posAnswer;
	public Dialogue posloop;

    private Animator animator;

    public Dialogue newQuest;

    Quests quests;
    public Quest quest;

    bool loop;
    bool touch;
    bool round1;

    private bool speakAble;
    public GameObject leader;

    public GameObject selectionController;

    void Start()
    {
        speakAble = false;
        quests = Quests.instance;
        loop = false;
        touch = true;
        round1 = false;
        animator = GetComponent<Animator>();
    }

    public void TriggerDialogue()
    {
        if (leader.GetComponent<LeaderTrigger>().GetIntroDone()){
            if (touch)
            {
                selectionController.GetComponent<SelectionController>().SetTouch(false);
                touch = false;
                if (loop)
                {
                    StartCoroutine(AudioDialoguePositiveLoop());
                }
                else
                {
                    //FindObjectOfType<MainQuestManager>().StartDialogue(dialogue);
                    StartCoroutine(AudioDialogue());
                    if (!round1)
                    {
                        quests.AddQuest(quest);
                        FindObjectOfType<DialogueManager>().StartDialogue(newQuest);
                        round1 = true;
                    }

                }
            }
           
        }
        else
        {
            if (touch)
            {
                selectionController.GetComponent<SelectionController>().SetTouch(false);
                touch = false;
                StartCoroutine(AudioDialogueIntro());
            }
        }
        //FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
       // StartCoroutine(AudioDialogue());
    }

    IEnumerator AudioDialogue()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("SorgenKind");
        animator.SetInteger("AnimState", 1);
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogue);
        yield return new WaitForSeconds(5f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(4f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 2);
        speakAble = true;
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        

    }


    public void PositiveAnswer()
    {
        quest.done = true;
        quests.ChangeQuest(quest);
        //FindObjectOfType<DialogueManager>().StartDialogue(posAnswer);
        StartCoroutine(AudioDialoguePositive());
        loop = true;
    }

    IEnumerator AudioDialoguePositive()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("FreudenKind");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(posAnswer);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(5f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(7f);
        animator.SetInteger("AnimState", 2);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;

    }
    
    IEnumerator AudioDialoguePositiveLoop()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("FreudenKindLoop");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(posloop);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(3f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(5f);
        animator.SetInteger("AnimState", 2);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;

    }  
    
    IEnumerator AudioDialogueIntro()
    {
        FindObjectOfType<AudioManager>().Play("KleinesMädchenIntro");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(intro);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(5f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
    }
    
    public void SetSpeakAble(bool speakAble2)
    {
        speakAble = speakAble2;
    }

    public bool GetSpeakAble()
    {
        return speakAble;
    }
}
