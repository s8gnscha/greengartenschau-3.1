﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Skript für das handeln der Answerbox des Alten Manns (zweite Box). Funktionen laufen analog zu Answermananger
 */
public class MainQuestAnswer2 : MonoBehaviour
{
    public GameObject answerbox;
    public GameObject dialogue;
    // Start is called before the first frame update
    void Start()
    {
        answerbox.SetActive(false);
    }


    public void ActivateBox()
    {
        answerbox.SetActive(true);
        //animator.SetBool("IsOpen2", true);
    }

    public void PositiveAnswer()
    {
        //animator.SetBool("IsOpen2", false);
        answerbox.SetActive(false);
        dialogue.GetComponent<MainQuestTrigger>().EndDialogNegative();
    }

    public void NegativeAnswer()
    {
        //animator.SetBool("IsOpen2", false);
        answerbox.SetActive(false);
        dialogue.GetComponent<MainQuestTrigger>().EndDialogNegative2();
    }
}
