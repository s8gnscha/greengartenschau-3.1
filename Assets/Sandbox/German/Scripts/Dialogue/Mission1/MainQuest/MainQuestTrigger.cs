﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Verhalten und Audio beim alten Mann
 */
public class MainQuestTrigger : MonoBehaviour
{
    //Textsequenzen
    public Dialogue dialogue;
    public Dialogue dialoguepos;
    public Dialogue dialoguedis;
    public Dialogue dialogueneg;
    public Dialogue dialogueneg2;
    public Dialogue dialoguefin;
    public Dialogue dialogueloop;
    public Dialogue dialoguefinloop;
    public Dialogue dialogueintro;
    
    //Quest
    public Dialogue newQuest;
    public Dialogue done;
    Quests quests;
    public Quest quest;
    
    // Gameobjects und aktiveren und deaktiveren
    public GameObject answerBox1;
    public GameObject answerBox2;

    public GameObject supporter;
    public GameObject selectionController;

    public GameObject leader;
    public Text signtext;

    bool loop;
    bool touch;

    private Animator animator;

    private bool positive;
    private bool speakAble;
    

    public GameObject signature;
    

    void Start()
    {
        speakAble = false;
        //signtext.text = "0/1";
        quests = Quests.instance;
        loop = false;
        touch = true;
        positive = false;
        animator = GetComponent<Animator>();
    }

    /**
     * Funktion, welche aufgerufen wird, wenn der alte Mann berührt wird
     */
    public void TriggerDialogue()
    {
        if (leader.GetComponent<LeaderTrigger>().GetIntroDone())
        {
            if (touch)
            {
                if (!positive)
                {
                    selectionController.GetComponent<SelectionController>().SetTouch(false);
                    touch = false;
                    if (loop)
                    {
                        StartCoroutine(AudioDialogueLoop());
                    }
                    else
                    {
                        //FindObjectOfType<MainQuestManager>().StartDialogue(dialogue);
                        StartCoroutine(AudioDialogue());
                    }
                }
                else
                {
                    StartCoroutine(AudioDialoguePositiveLoop());
                }
            }
        }
        else
        {
            if (touch)
            {
                selectionController.GetComponent<SelectionController>().SetTouch(false);
                touch = false;
                StartCoroutine(AudioDialogueIntro());
            }
        }
    }

    /**
     * Verzweigung, wenn man bei der ersten Answerbox die erste Möglichkeit nimmt
     */
    public void EndDialogPositive()
    {
        // FindObjectOfType<DialogueManager>().StartDialogue(dialoguepos);
        StartCoroutine(AudioDialoguePositive());
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;

    }

    /**
     * Verzweigung, wenn man bei der ersten Answerbox die zweite Möglichkeit nimmt
     */
    public void EndDialogDiscuss()
    {
        //FindObjectOfType<MainQuestManager2>().StartDialogue(dialoguedis);
        StartCoroutine(AudioDialogueDiscuss());
    }

    /**
     * Verzweigung, wenn man bei der zweiten Answerbox die erste Möglichkeit nimmt
     */
    public void EndDialogNegative()
    {
        //FindObjectOfType<DialogueManager>().StartDialogue(dialogueneg);
        
        quests.AddQuest(quest);
        FindObjectOfType<DialogueManager>().StartDialogue(newQuest);
        supporter.GetComponent<SupportTrigger>().SetStart(true);
        selectionController.GetComponent<SelectionController>().SetTouchable2(true);
        StartCoroutine(AudioDialogueNegative());
    }

    /**
     * Verzweigung, wenn man bei der zweiten Answerbox die zweite Möglichkeit nimmt
     */
    public void EndDialogNegative2()
    {
        //FindObjectOfType<DialogueManager>().StartDialogue(dialogueneg2);
        
        quests.AddQuest(quest);
        FindObjectOfType<DialogueManager>().StartDialogue(newQuest);
        supporter.GetComponent<SupportTrigger>().SetStart(true);
        selectionController.GetComponent<SelectionController>().SetTouchable2(true);
        StartCoroutine(AudioDialogueNegative2());
    }

    /**
     * Verzweigung, wenn man die Mission geschafft hat
     */
    public void EndDialogFinal()
    {
        quest.done = true;
        quests.ChangeQuest(quest);
        //FindObjectOfType<DialogueManager>().StartDialogue(dialoguefin);
        StartCoroutine(AudioDialogueFinal());
        signature.GetComponent<SignatureCounter>().IncreaseCounter();
        positive = true;
    }

/**
 * Audiosequenz, wenn man ihn nach der Demo anspricht
 */
    IEnumerator AudioDialogue()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("AlterSackAfterIntroIntro");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogue);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(2f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        answerBox1.SetActive(true);

    }

/**
 * Audiosequenzen (Siehe Endialoguefunktionen)
 */
    IEnumerator AudioDialoguePositive()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("AlterSackPositive");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguepos);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(4f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
    }

    IEnumerator AudioDialogueDiscuss()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("AlterSackDiskussionAnfang");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguedis);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(6f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(6f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        answerBox2.SetActive(true);

    }

    IEnumerator AudioDialogueNegative()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("AlterSackDiskussionNegative");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueneg);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(4f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(4f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
        

    }

    IEnumerator AudioDialogueNegative2()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("AlterSackDiskussionNegative2");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueneg2);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(3f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(4f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(3f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
        
    }

    IEnumerator AudioDialogueFinal()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("AlterSackEnde");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguefin);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(2f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(4f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(4f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        touch = true;
    }

    /**
 * Audiosequenz, wenn man ihn während der Mission mehr als einmal anspricht
 */
    IEnumerator AudioDialogueLoop()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("AlterSackLoop");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueloop);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(9f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
        speakAble = true;
    } 
    
    /**
    * Audiosequenz, wenn man ihn vor der Demo anspricht
    */
    IEnumerator AudioDialogueIntro()
    {
        FindObjectOfType<AudioManager>().Play("AlterSackIntro");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueintro);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(5f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
    }
    
    /**
 * Audiosequenz, wenn man ihn nach der Mission mehr als einmal anspricht
 */
    IEnumerator AudioDialoguePositiveLoop()
    {
        FindObjectOfType<AudioManager>().Play("AlterSackPositiveLoop");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguefinloop);
        animator.SetInteger("AnimState", 1);
        yield return new WaitForSeconds(6f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		touch = true;
    }
    
    /**
 * notwendig beim falken
 */
    public void SetSpeakAble(bool speakAble2)
    {
        speakAble = speakAble2;
    }

    public bool GetSpeakAble()
    {
        return speakAble;
    }

    /**
     * notwendig für den supporter für positive loop
     */
    public bool GetPositive()
    {
        return positive;
    }
}
