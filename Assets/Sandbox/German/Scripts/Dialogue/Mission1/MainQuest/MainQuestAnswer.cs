﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Skript für das handeln der Answerbox des Alten Manns (erste Box). Funktionen laufen analog zu Answermananger
 */
public class MainQuestAnswer : MonoBehaviour
{
    public GameObject answerbox;
    public GameObject dialogue;
    // Start is called before the first frame update
    void Start()
    {
        answerbox.SetActive(false);
    }


    public void ActivateBox()
    {
        answerbox.SetActive(true);
        //animator.SetBool("IsOpen2", true);
    }

    public void PositiveAnswer()
    {
        //animator.SetBool("IsOpen2", false);
        answerbox.SetActive(false);
        dialogue.GetComponent<MainQuestTrigger>().EndDialogPositive();
    }

    public void NegativeAnswer()
    {
        //animator.SetBool("IsOpen2", false);
        answerbox.SetActive(false);
        dialogue.GetComponent<MainQuestTrigger>().EndDialogDiscuss();
    }
}
