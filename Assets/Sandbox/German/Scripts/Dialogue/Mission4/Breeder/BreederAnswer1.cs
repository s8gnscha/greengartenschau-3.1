using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Handeln der ersten Answerboxen. Läuft analog zu den Answerboxen von Mission 1 und Intro
 */
public class BreederAnswer1 : MonoBehaviour
{
    public GameObject answerbox;
    public GameObject dialogue;
    // Start is called before the first frame update
    void Start()
    {
        answerbox.SetActive(false);
    }


    public void ActivateBox()
    {
        answerbox.SetActive(true);
        //animator.SetBool("IsOpen2", true);
    }

    public void PositiveAnswer()
    {
        //animator.SetBool("IsOpen2", false);
        answerbox.SetActive(false);
        dialogue.GetComponent<BreederTrigger>().EndDialogDiscuss1();
    }

    public void NegativeAnswer()
    {
        //animator.SetBool("IsOpen2", false);
        answerbox.SetActive(false);
        dialogue.GetComponent<BreederTrigger>().EndDialogDiscuss2();
    }
}
