using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/**
 * Verhalten und Audio beim Züchter. Läuft nach dem selben Prinzip wie Mission 1 und Intro
 */
public class BreederTrigger : MonoBehaviour
{
    public Dialogue dialogue;
    public Dialogue dialoguepos;
    public Dialogue dialoguedis;
    public Dialogue dialogueneg;
    public Dialogue dialogueneg2;
    public Dialogue dialoguefin;
    public Dialogue dialogueloop;
    public Dialogue dialoguefinloop;
    public Dialogue dialogueintro;


      public Dialogue newQuest;
      public GameObject leader;
      private Animator animator;


    Quests quests;
    public Quest quest;
    public GameObject answerBox1;
    public GameObject answerBox2;

    
    public GameObject selectionController;
    
    public GameObject signature;
    bool loop;
    bool touch;

    private bool done;

    private bool dialogueDone;

    private bool positive;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        dialogueDone = false;
        done = false;
        quests = Quests.instance;
        loop = false;
        touch = true;
        positive = false;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetDone()
    {
        done = true;
    }

    public bool GetDialogueDone()
    {
        return dialogueDone;
    }
    
    public void TriggerDialogue()
    {
        if (leader.GetComponent<LeaderTrigger>().GetIntroDone())
        {
            if (touch)
            {
                selectionController.GetComponent<SelectionController>().SetTouch(false);
                touch = false;
                if (!positive)
                {
                    if (!done)
                    {
                        if (loop)
                        {
                            StartCoroutine(AudioDialogueLoop());
                        }
                        else
                        {
                            //FindObjectOfType<MainQuestManager>().StartDialogue(dialogue);
                            StartCoroutine(AudioDialogue());
                        }
                    }
                    else
                    {
                        EndDialogFinal();
                    }
                }
            }
            else
            {
                StartCoroutine(AudioDialoguePositiveLoop());
            }
        }
        else
        {
            if (touch)
            {
                selectionController.GetComponent<SelectionController>().SetTouch(false);
                touch = false;
                StartCoroutine(AudioDialogueIntro());
            }
        }
    }
    
    public void EndDialogDiscuss1()
    {
        //FindObjectOfType<MainQuestManager2>().StartDialogue(dialoguedis);
        StartCoroutine(AudioDialogueDiscuss1());
    }
    
    public void EndDialogDiscuss2()
    {
        //FindObjectOfType<MainQuestManager2>().StartDialogue(dialoguedis);
        StartCoroutine(AudioDialogueDiscuss2());
    }
    
    public void EndDialogNegative()
    {
        //FindObjectOfType<DialogueManager>().StartDialogue(dialogueneg);
        
        quests.AddQuest(quest);
        FindObjectOfType<DialogueManager>().StartDialogue(newQuest);
        dialogueDone = true;
        StartCoroutine(AudioDialogueNegative());
    }

    public void EndDialogNegative2()
    {
        //FindObjectOfType<DialogueManager>().StartDialogue(dialogueneg2);
        
        quests.AddQuest(quest);
        FindObjectOfType<DialogueManager>().StartDialogue(newQuest);
        dialogueDone = true;
        StartCoroutine(AudioDialogueNegative2());
    }
    
    public void EndDialogFinal()
    {
        quest.done = true;
        quests.ChangeQuest(quest);
        //FindObjectOfType<DialogueManager>().StartDialogue(dialoguefin);
        StartCoroutine(AudioDialogueFinal());
        signature.GetComponent<SignatureCounter>().IncreaseCounter();
        
        positive = true;
    }
    
    IEnumerator AudioDialogue()
    {
        
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("ReederBeginning");
        animator.SetInteger("AnimState", 2);
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogue);
        yield return new WaitForSeconds(3f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        answerBox1.SetActive(true);
        
    }
    
    IEnumerator AudioDialogueDiscuss1()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("ReederDiscuss1");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguepos);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(3f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        answerBox2.SetActive(true);

    }
    
    IEnumerator AudioDialogueDiscuss2()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("ReederDiscuss2");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguedis);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(5f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        answerBox2.SetActive(true);

    }
    
    IEnumerator AudioDialogueNegative()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("ReederMission1");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueneg);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(3f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        yield return new WaitForSeconds(5f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
        dialogueDone = true;
        

    }

    IEnumerator AudioDialogueNegative2()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("ReederMission2");
        animator.SetInteger("AnimState", 2);
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueneg2);
        yield return new WaitForSeconds(3f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        yield return new WaitForSeconds(5f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
        dialogueDone = true;
        
    }
    
    IEnumerator AudioDialogueLoop()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("ReederLoop");
        animator.SetInteger("AnimState", 2);
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueloop);
        yield return new WaitForSeconds(2f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;

    }
    
    IEnumerator AudioDialogueIntro()
    {
        FindObjectOfType<AudioManager>().Play("BreederIntro");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueintro);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(5f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        
    }
    
    IEnumerator AudioDialogueFinal()
    {

        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("ReederWin");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguefin);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(5f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        yield return new WaitForSeconds(5f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 1);
        selectionController.GetComponent<SelectionController>().SetDuckDone(true);
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;

    }
    
    IEnumerator AudioDialoguePositiveLoop()
    {
        FindObjectOfType<AudioManager>().Play("ReederPositiveLoop");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguefinloop);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(3f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
    }
}
