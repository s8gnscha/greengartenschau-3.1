using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * Verhalten und Audio des Doktors am Teich
 */
public class Doctor2Trigger : MonoBehaviour
{

	public Dialogue solution;

    public GameObject selectionController;
	bool touch;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        touch=true;
        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EndDialogueLoop()
    {
		if(touch){
            selectionController.GetComponent<SelectionController>().SetTouch(false);
            touch = false;
            StartCoroutine(AudioDialogueLoop());
		}
    }

    IEnumerator AudioDialogueLoop()
    {
        FindObjectOfType<AudioManager>().Play("DoctorSolution");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(solution);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(5f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 1);
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
    }
}
