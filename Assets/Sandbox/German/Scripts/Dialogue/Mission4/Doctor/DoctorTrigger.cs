using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/**
 * Verhalten und Audio vom Doktor. läuft analog zu den Leuten in Mission 1 und Intro
 */
public class DoctorTrigger : MonoBehaviour
{
	public Dialogue dialogue;
    public Dialogue dialoguepos;
    public Dialogue dialoguedis;
    public Dialogue dialogueneg;
    public Dialogue dialogueneg2;
    public Dialogue dialoguefin;
    public Dialogue dialogueloop;
    public Dialogue dialogueintro;
	public Dialogue dialog1outof3;
	public Dialogue dialog2outof3;
	public Dialogue dialog3outof3;
	public Dialogue dialogueintroloop;





    public Dialogue newQuest;

    public Dialogue vanish;
    public GameObject leader;

    public GameObject breeder;

    public GameObject selectionController;
    
    Quests quests;
    public Quest quest;
    public GameObject answerBox1;
    public GameObject answerBox2;
    private Animator animator;
   
    public GameObject doctor;
    public GameObject doctor2;
    
    
    

	bool speakAble;
    bool loop;
    bool touch;
    int signature = 0;
    private int itemscount = 0;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
		speakAble=false;
        doctor2.SetActive(false);
        quests = Quests.instance;
        loop = false;
        touch = true;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void TriggerDialogue()
    {
        if (leader.GetComponent<LeaderTrigger>().GetIntroDone())
        {
            if (breeder.GetComponent<BreederTrigger>().GetDialogueDone())
            {
                if (touch)
                {
                    selectionController.GetComponent<SelectionController>().SetTouch(false);
                    touch = false;
                    if (loop)
                    {
                        StartCoroutine(AudioDialogueLoop());
                    }
                    else
                    {
                        //FindObjectOfType<MainQuestManager>().StartDialogue(dialogue);
                        StartCoroutine(AudioDialogue());
                    }

                }
            }
            else
            {
                if (touch)
                {
                    StartCoroutine(AudioDialogueAfterIntroLoop());
                }
            }
        }
        else
        {
            if (touch)
            {
                selectionController.GetComponent<SelectionController>().SetTouch(false);
                touch = false;
                StartCoroutine(AudioDialogueIntro());
            }
        }
    }
    
    public void EndDialogDiscuss1()
    {
        //FindObjectOfType<MainQuestManager2>().StartDialogue(dialoguedis);
        StartCoroutine(AudioDialogueDiscuss1());
    }
    
    public void EndDialogDiscuss2()
    {
        //FindObjectOfType<MainQuestManager2>().StartDialogue(dialoguedis);
        StartCoroutine(AudioDialogueDiscuss2());
    }
    
    public void EndDialogNegative()
    {
        //FindObjectOfType<DialogueManager>().StartDialogue(dialogueneg);
        
        quests.AddQuest(quest);
        FindObjectOfType<DialogueManager>().StartDialogue(newQuest);
        
        
        StartCoroutine(AudioDialogueNegative());
    }

    public void EndDialogNegative2()
    {
        //FindObjectOfType<DialogueManager>().StartDialogue(dialogueneg2);
        
        quests.AddQuest(quest);
        FindObjectOfType<DialogueManager>().StartDialogue(newQuest);
        
        
        StartCoroutine(AudioDialogueNegative2());
    }
    
    IEnumerator AudioDialogue()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DoctorBeginning");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogue);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(3f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        answerBox1.SetActive(true);

    }
    
    IEnumerator AudioDialogueDiscuss1()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DoctorDiscuss1");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguepos);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(5f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        answerBox2.SetActive(true);

    }
    
    IEnumerator AudioDialogueDiscuss2()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DoctorDiscuss2");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialoguedis);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(7f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        answerBox2.SetActive(true);

    }
    
    IEnumerator AudioDialogueNegative()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DoctorMission1");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueneg);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(11f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(10f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(10f);
		animator.SetInteger("AnimState", 0);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
        

    }

    IEnumerator AudioDialogueNegative2()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DoctorMission2");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueneg2);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(4f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(9f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(10f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
		yield return new WaitForSeconds(10f);
		animator.SetInteger("AnimState", 0);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
        
    }
    
    IEnumerator AudioDialogueLoop()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("DoctorLoop");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueloop);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(3f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
		speakAble=true;

    }
    
    IEnumerator AudioDialogueIntro()
    {
        FindObjectOfType<AudioManager>().Play("DoctorIntro");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueintro);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(5f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
    }

    IEnumerator AudioDialogueAfterIntroLoop()
    {
        FindObjectOfType<AudioManager>().Play("DoctorAfterIntroLoop");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueintroloop);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(3f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        animator.SetInteger("AnimState", 0);
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
    }

	IEnumerator AudioDoctor1outof3(){
	    touch=false;
	    FindObjectOfType<AudioManager>().Play("Doctor1outof3");
	    FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialog1outof3);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(2f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
	}

	IEnumerator AudioDoctor2outof3(){
	    touch=false;
	    FindObjectOfType<AudioManager>().Play("Doctor2outof3");
	    FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialog2outof3);
        animator.SetInteger("AnimState", 2);
        yield return new WaitForSeconds(3f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
	}

	IEnumerator AudioDoctor3outof3(){
	    touch=false;
        quest.done = true;
        quests.ChangeQuest(quest);
	    FindObjectOfType<AudioManager>().Play("Doctor3outof3");
	    FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialog3outof3);
        animator.SetInteger("AnimState", 2);
	    yield return new WaitForSeconds(8f);
        animator.SetInteger("AnimState", 0);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
	}

    public void IncreaseItemsCount()
    {
        itemscount++;

        if (itemscount == 1)
        {
			speakAble=false;
			
            StartCoroutine(AudioDoctor1outof3());
			
		
        }
        if (itemscount == 2)
        {
			speakAble=false;
			
            StartCoroutine(AudioDoctor2outof3());
			
			
        }
        if (itemscount == 3)
        {
			speakAble=false;
			
            StartCoroutine(AudioDoctor3outof3());
			
            FindObjectOfType<DialogueManager>().StartDialogue(vanish);
            breeder.GetComponent<BreederTrigger>().SetDone();
            doctor2.SetActive(true);
            doctor.SetActive(false);

        }
    }
    
 public void SetSpeakAble(bool speakAble2)
    {
        speakAble = speakAble2;
    }

    public bool GetSpeakAble()
    {
        return speakAble;
    }

}
