using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Handeln der zweiten Answerboxen. Läuft analog zu den Answerboxen von Mission 1 und Intro
 */
public class DoctorAnswer2 : MonoBehaviour
{
    public GameObject answerbox;
    public GameObject dialogue;
    // Start is called before the first frame update
    void Start()
    {
        answerbox.SetActive(false);
    }


    public void ActivateBox()
    {
        answerbox.SetActive(true);
        //animator.SetBool("IsOpen2", true);
    }

    public void PositiveAnswer()
    {
        //animator.SetBool("IsOpen2", false);
        answerbox.SetActive(false);
        dialogue.GetComponent<DoctorTrigger>().EndDialogNegative();
    }

    public void NegativeAnswer()
    {
        //animator.SetBool("IsOpen2", false);
        answerbox.SetActive(false);
        dialogue.GetComponent<DoctorTrigger>().EndDialogNegative2();
    }
}
