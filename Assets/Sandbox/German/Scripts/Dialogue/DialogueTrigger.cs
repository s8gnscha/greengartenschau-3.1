﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * nicht in Verwendung
 */
public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;
    public Dialogue dialoguepos;
    public Dialogue dialogueneg;
    
    

    public void TriggerDialogue()
    {
        
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
        
    }

    public void EndDialogPositive()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialoguepos);
    }

    public void EndDialogNegative()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogueneg);
    }


   
    
}
