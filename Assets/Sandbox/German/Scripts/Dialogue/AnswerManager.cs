﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * Skript für das handeln der Answerbox des Anführers der Demo
 */
public class AnswerManager : MonoBehaviour
{
    public Animator animator;
    public GameObject answerbox;
    public GameObject dialogue;
    // Start is called before the first frame update
    void Start()
    {
        answerbox.SetActive(false);
    }

/**
 * Funktion, um die Answerbox zu aktiveren
 */
    public void ActivateBox()
    {
        answerbox.SetActive(true);
        //animator.SetBool("IsOpen2", true);
    }

/**
 * Fall, wenn man die erste Möglichkeit bei der Answerbox nutzt
 */
    public void PositiveAnswer()
    {
        //animator.SetBool("IsOpen2", false);
        answerbox.SetActive(false);
        dialogue.GetComponent<LeaderTrigger>().EndDialogPositive();
    }

/**
 * Fall, wenn man die zweite Möglichkeit bei der Answerbox nutzt
 */
    public void NegativeAnswer()
    {
        //animator.SetBool("IsOpen2", false);
        answerbox.SetActive(false);
        dialogue.GetComponent<LeaderTrigger>().EndDialogNegative();
    }
}
