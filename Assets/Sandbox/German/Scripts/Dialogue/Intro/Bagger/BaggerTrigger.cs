﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Verhalten und Audio beim Bagger
 */
public class BaggerTrigger : MonoBehaviour
{
    public Dialogue dialogue;
    public GameObject selectionController;

    private bool touch;


    void Start()
    {
        touch = true;
    }

    /**
     * Funktion, wenn der Bagger berührt wird
     */
    public void TriggerDialogue()
    {
        if (touch)
        {
            selectionController.GetComponent<SelectionController>().SetTouch(false);
            touch = false;
            //FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
            StartCoroutine(AudioDialogue());
        }
    }

    /**
     * Das Handeln der Audiospur vom Bagger
     */
    IEnumerator AudioDialogue()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("Bagger");
		FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogue);
        yield return new WaitForSeconds(5f);
		FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;

    }


}
