﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Verhalten und Audio beim ersten Demostrant
 */
public class Demonstrant1Trigger : MonoBehaviour
{
    public Dialogue dialogue;
    
    public GameObject selectionController;

    private bool touch;


    void Start()
    {
        touch = true;
    }



    /**
     * Funktion, wenn der Demonstrant berührt wird
     */
    public void TriggerDialogue()
    {
        if (touch)
        {
            selectionController.GetComponent<SelectionController>().SetTouch(false);
            touch = false;
            //FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
            StartCoroutine(AudioDialogue());
        }
    }

    /**
     * Das Handeln der Audiospur vom Demonstrant
     */
    IEnumerator AudioDialogue()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("Demonstrant2");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogue);
        yield return new WaitForSeconds(3f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;

    }
}
