﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/**
 * Verhalten und Audio beim Anführer der Demo
 */
public class LeaderTrigger : MonoBehaviour
{
//Textsequenzen
    public GameObject selectionController;
    public Dialogue dialogue;

    public Dialogue pos;
    public Dialogue neg;
    public Dialogue dialogueloop;

    public Dialogue intro;
//GameObjects zum spawnen
    public GameObject inventorybutton;
    public GameObject questbutton;
    
	public GameObject flower;



    public GameObject answerBox1;
    bool loop;
    bool touch;

    private bool introDone;

    void Start()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(intro);
        loop = false;
        touch = true;
        introDone = false;

    }

/**
 * Funktion, die genutzt wird, wenn der Anführer berührt wird
 */
    public void TriggerDialogue()
    {
        if (touch) {
            selectionController.GetComponent<SelectionController>().SetTouch(false);
            touch = false;
        if (loop)
            {
                StartCoroutine(AudioDialogueLoop());
            }
            else { //FindObjectOfType<IntroManager>().StartDialogue(dialogue);
                StartCoroutine(AudioDialogue());
            }
            
        }
    }

/**
 * Zweig, der genutzt wird, falls man ja bei der Answerbox gibt
 */
    public void EndDialogPositive()
    {
        //FindObjectOfType<DialogueManager>().StartDialogue(pos);
        StartCoroutine(AudioDialoguePositive());
        inventorybutton.SetActive(true);
        questbutton.SetActive(true);
        introDone = true;
    }
    
/**
 * Zweig, der genutzt wird, wenn man nein bei der Answerbox gibt
 */
    public void EndDialogNegative()
    {
        //FindObjectOfType<IntroManager>().StartDialogue(neg);
        StartCoroutine(AudioDialogueNegative());
    }


/**
 * Audiosequenz für das Intro
 */
    IEnumerator AudioDialogue()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("LeaderIntro");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogue);
        yield return new WaitForSeconds(5f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        yield return new WaitForSeconds(5f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        yield return new WaitForSeconds(7f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        yield return new WaitForSeconds(5f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        answerBox1.GetComponent<AnswerManager>().ActivateBox();

    }

/**
 * Audiosequenz für den Loop (wenn er mehr als einmal berührt wird)
 */
    IEnumerator AudioDialogueLoop()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("LeaderLoop");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(dialogueloop);
        yield return new WaitForSeconds(2f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
    }

/**
 * Audiosequenz, wenn man ja sagt bei der Answerbox
 */
    IEnumerator AudioDialoguePositive()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("LeaderYes");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(pos);
        yield return new WaitForSeconds(9f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        yield return new WaitForSeconds(6f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        yield return new WaitForSeconds(5f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        selectionController.GetComponent<SelectionController>().SetTouch(true);
        touch = true;
        loop = true;
    }

/**
 * Audiosequenz, wenn man nein sagt bei der Answerbox
 */
    IEnumerator AudioDialogueNegative()
    {
        //text.text = "Done";
        FindObjectOfType<AudioManager>().Play("LeaderNo");
        FindObjectOfType<DialogueManagerAutomatic>().StartDialogue(neg);
        yield return new WaitForSeconds(5f);
        FindObjectOfType<DialogueManagerAutomatic>().DisplayNextSentence();
        answerBox1.SetActive(true);
    }

/**
 * Bool, das returned, ob das intro vorbei ist
 */
    public bool GetIntroDone()
    {
        return introDone;
    }
}
