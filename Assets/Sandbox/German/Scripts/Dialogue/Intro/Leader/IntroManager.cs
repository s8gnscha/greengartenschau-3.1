﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/**
 * Funktionen bei der ursprünglichen Introbox. Wird nicht verwendet
 */
public class IntroManager : MonoBehaviour
{
    public Text nameText;
    public Text dialogueText;

    public Animator animator;

    public GameObject dialoguebox;

    private Queue<string> sentences;
    public GameObject answerBox;


    // Use this for initialization
    void Start()
    {
        sentences = new Queue<string>();
		
        dialoguebox.SetActive(false);

    }

    public void StartDialogue(Dialogue dialogue)
    {
        dialoguebox.SetActive(true);

        //animator.SetBool("IsOpen", true);

        nameText.text = dialogue.name;

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    void EndDialogue()
    {
        dialoguebox.SetActive(false);
        answerBox.SetActive(true);

        //animator.SetBool("IsOpen", false);

    }
}
