using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Skript was genutzt wird, um den Dialoguebox zu handeln (den ohne den Continuebutton)
 */
public class DialogueManagerAutomatic : MonoBehaviour
{
    public Text nameText;
    public Text dialogueText;

    public Animator animator;

    public GameObject dialoguebox;

    private Queue<string> sentences;
    


    // Use this for initialization
    void Start()
    {
        sentences = new Queue<string>();
		
        dialoguebox.SetActive(false);

    }

    /**
	 * Startet die Textsequenz in Dialoguebox und aktiviert die Dialoguebox
	 */
    public void StartDialogue(Dialogue dialogue)
    {
        dialoguebox.SetActive(true);

        //animator.SetBool("IsOpen", true);

        nameText.text = dialogue.name;

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    /**
	 * Zeigt die nächste sequenz von dialogue
	 */
    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    /**
	 * schreibt den Text in Dialoguebox
	 */
    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    /**
	 * beendet die Dialoguebox
	 */
    void EndDialogue()
    {
        dialoguebox.SetActive(false);
        //answerBox.SetActive(true);

        //animator.SetBool("IsOpen", false);

    }
}
