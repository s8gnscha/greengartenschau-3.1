﻿
using UnityEngine;


/**
 * Beschreibung des Item Objekts
 */

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
	{

		new public string itemName = "New Item";    // Name of the item
		public Sprite icon = null;              // Item icon
		public bool isDefaultItem = false;      // Is the item default wear?
	}
