using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Funktion, um das Item wegzumachen. Wird nicht verwendet
 */

public class ItemDestroy : MonoBehaviour
{

    public GameObject item;
    public void Destroy()
    {
        item.SetActive(false);
    }
}
