using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * Handeln des Inventarsystems
 */
public class Inventory : MonoBehaviour
{


    public List<Item> items=new List<Item>();


    #region Singleton

    public static Inventory instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }

        instance = this;
    }

    #endregion


    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int space = 16;	// Amount of slots in inventory


/**
 * Item wird in die Inventarliste hinzugefügt
 */
    public void AddItem(Item item)
    {
        if (items.Count >= space)
        {
            
            return ;
        }
        items.Add(item);
        Debug.Log("picked up successful");
        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    
}

    /**
     * Item wird aus der Inventarliste entfernt
     */
    public void RemoveItem(Item item)
    {
        items.Remove(item);
        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    
}

 
}

