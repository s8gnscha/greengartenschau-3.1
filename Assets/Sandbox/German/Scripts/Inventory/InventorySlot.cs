using UnityEngine;
using UnityEngine.UI;

/**
 * Handeln eines einzelnen Inventarslots
 */
public class InventorySlot : MonoBehaviour
{
    public Image icon;

    public Dialogue error;
    public GameObject sideQuest;
    public GameObject mainQuest;

    public GameObject doctor;
    public GameObject getter;
    
    public GameObject getter2;
    
    public GameObject getter3;
    
    

    Item item;

    /**
     * Hinzufügen eines Items in den Slot
     */
    public void AddItem (Item newItem)
    {
        item = newItem;
        icon.sprite = item.icon;
        icon.enabled = true;
        
    }

    /**
     * Entfernen eines Items in den Slot
     */
    public void ClearSlot()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
    }


/**
 * Funktion für das Handleln, wenn ein Item benutzt wird
 */
    public void UseItem()
    {
        if(item != null)
        {
            if (item.itemName == "Flower")
            {
                if (sideQuest.GetComponent<SideQuestTrigger>().GetSpeakAble())
                {
                    if (sideQuest.activeSelf)
                    {
                        Inventory.instance.RemoveItem(item);
                        sideQuest.GetComponent<SideQuestTrigger>().PositiveAnswer();
                    }  
                }
                else
                {
                    FindObjectOfType<DialogueManager>().StartDialogue(error);
                }
                
            }
            else if (item.itemName == "Bird")
            {
                if (mainQuest.GetComponent<MainQuestTrigger>().GetSpeakAble())
                {
                    if (mainQuest.activeSelf) {
                        Inventory.instance.RemoveItem(item);
                        mainQuest.GetComponent<MainQuestTrigger>().EndDialogFinal();
                    } 
                }
                else
                {
                    FindObjectOfType<DialogueManager>().StartDialogue(error);
                }
               
            }

            else if (item.itemName == "Book")
            {
                if (doctor.GetComponent<DoctorTrigger>().GetSpeakAble())
                {
                    Inventory.instance.RemoveItem(item);
                    doctor.GetComponent<DoctorTrigger>().IncreaseItemsCount();
                }
                else
                {
                    FindObjectOfType<DialogueManager>().StartDialogue(error);
                }
            }
            else if (item.itemName == "Stetoscope")
            {
                if (doctor.GetComponent<DoctorTrigger>().GetSpeakAble())
                {
                    Inventory.instance.RemoveItem(item);
                    doctor.GetComponent<DoctorTrigger>().IncreaseItemsCount();
                }
                else
                {
                    FindObjectOfType<DialogueManager>().StartDialogue(error);
                }
            }
            else if (item.itemName == "Medical")
            {
                if (doctor.GetComponent<DoctorTrigger>().GetSpeakAble())
                {
                    Inventory.instance.RemoveItem(item);
                    doctor.GetComponent<DoctorTrigger>().IncreaseItemsCount();
                }
                else
                {
                    FindObjectOfType<DialogueManager>().StartDialogue(error);
                }
            }
            else if (item.itemName == "Food")
            {
                if (getter.GetComponent<Getter1Trigger>().GetSpeakAble())
                {
                    Inventory.instance.RemoveItem(item);
                    getter.GetComponent<Getter1Trigger>().PositiveAnswer();
                }
                else
                {
                    FindObjectOfType<DialogueManager>().StartDialogue(error);
                }
            }
            else if (item.itemName == "Food2")
            {
                if (getter2.GetComponent<Getter2Trigger>().GetSpeakAble())
                {
                    Inventory.instance.RemoveItem(item);
                    getter2.GetComponent<Getter2Trigger>().PositiveAnswer();
                }
                else
                {
                    FindObjectOfType<DialogueManager>().StartDialogue(error);
                }
            }
            else if (item.itemName == "Food3")
            {
                if (getter3.GetComponent<Getter3Trigger>().GetSpeakAble())
                {
                    Inventory.instance.RemoveItem(item);
                    getter3.GetComponent<Getter3Trigger>().PositiveAnswer();
                }
                else
                {
                    FindObjectOfType<DialogueManager>().StartDialogue(error);
                }
            }
        }
    }
}
