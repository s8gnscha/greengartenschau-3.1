﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Animation, um das Inventar zu holen
 */
public class InventoryVisibility : MonoBehaviour
{


    bool visibility;
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        visibility = false;
        animator.SetBool("InventoryIsOpen", visibility);

    }
/**
 * Animation, wann das Inventar kommen oder gehen soll
 */
    public void SetWindow()
    {
        if (visibility)
        {
            visibility = false;
            animator.SetBool("InventoryIsOpen", visibility);
        }
        else
        {
            visibility = true;
            animator.SetBool("InventoryIsOpen", visibility);
        }
    }

 
}
