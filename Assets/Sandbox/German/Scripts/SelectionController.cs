﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/**
* wird verwendet, um das Berühren in AR zu regulieren
*/
public class SelectionController : MonoBehaviour
{
// Item, Inventory
	public Dialogue itemIntro;
    public Item[] items;
    Inventory inventory;

// die platzierten Objekte
    [SerializeField]
    private PlacementObject[] placedObjects;

//Ar Kamera
    [SerializeField]
    private Camera arCamera;

    private Vector2 touchPosition = default;
    GameObject dialogue;
    GameObject text;

//Items to find
    public GameObject flower;

    public GameObject medical;

    public GameObject stetoscope;

    public GameObject books;

    //Leader

    public GameObject leader;

//People to set speakable to false
    public GameObject sideQuest;
    public GameObject mainQuest;
    public GameObject doctor;


    bool touchable;
    bool touchableAll;
	bool duckDone;

    private bool touchable2;

    // Start is called before the first frame update
    void Start()
    {
        text = GameObject.Find("Text");
        inventory = Inventory.instance;
        touchable = false;
        touchable2 = false;
        touchableAll = true;
        
    }

    // Update is called once per frame
	//Algorithmus folgendermaßen: wenn man auf die Position drückt, dann wird geschaut, welche Objekte auf der Fläche sind.
	//Das vorderste Objekt wird ausgewählt (siehe nächste Funktion)
    void Update()
    {
        if (touchableAll)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                touchPosition = touch.position;

                if (touch.phase == TouchPhase.Began)
                {
                    Ray ray = arCamera.ScreenPointToRay(touch.position);
                    RaycastHit hitObject;
                    if (Physics.Raycast(ray, out hitObject))
                    {
                        PlacementObject placementObject = hitObject.transform.GetComponent<PlacementObject>();
                        if (placementObject != null)
                        {
	                        
                            InteractionWithPlacedObject(placementObject);

                        }
                    }
                }
            }
        }
    }

// Es wird geschaut, welcher virtuelle Objekt ausgewählt wurde. die entsprechende Funktion wird ausgeführt
    void InteractionWithPlacedObject(PlacementObject selected)
    {

        if (selected == placedObjects[0])
        {
	        if (leader.GetComponent<LeaderTrigger>().GetIntroDone())
	        {
		        FindObjectOfType<AudioManager>().Play("Grab");
		        inventory.AddItem(items[0]);
		        flower.SetActive(false);
				sideQuest.GetComponent<SideQuestTrigger>().SetSpeakAble(false);
	        }
	        else
	        {
		        FindObjectOfType<DialogueManager>().StartDialogue(itemIntro);
	        }

        }
        if (selected == placedObjects[1])
        {
            selected.GetComponent<MainQuestTrigger>().TriggerDialogue();
        }
        if (selected == placedObjects[2])
        {
            if (touchable2)
            {
                if (!touchable)
                {
                    FindObjectOfType<AudioManager>().Play("Photo");
                    inventory.AddItem(items[1]);
                    touchable = true;
					mainQuest.GetComponent<MainQuestTrigger>().SetSpeakAble(false);
                }
                else
                {
                    FindObjectOfType<AudioManager>().Play("Bird");
                }
            }
            else
            {
                FindObjectOfType<AudioManager>().Play("Bird");
            }
        }
        if (selected == placedObjects[3])
        {
            selected.GetComponent<SupportTrigger>().TriggerDialogue();
        }
        if (selected == placedObjects[4])
        {
            selected.GetComponent<SideQuestTrigger>().TriggerDialogue();
        }
        if (selected == placedObjects[5])
        {
            selected.GetComponent<BaggerTrigger>().TriggerDialogue();
        }
        if (selected == placedObjects[6])
        {
            selected.GetComponent<LeaderTrigger>().TriggerDialogue();
        }
        if (selected == placedObjects[7])
        {
            selected.GetComponent<Demonstrant1Trigger>().TriggerDialogue();
        }
        if (selected == placedObjects[8])
        {
            selected.GetComponent<Demonstrant2Trigger>().TriggerDialogue();
        }
		if (selected == placedObjects[9])
		{
			selected.GetComponent<DoctorTrigger>().TriggerDialogue();
		}
		if (selected == placedObjects[10])
		{
			selected.GetComponent<BreederTrigger>().TriggerDialogue();
		}
		if (selected == placedObjects[11])
		{

			if (leader.GetComponent<LeaderTrigger>().GetIntroDone())
			{
				FindObjectOfType<AudioManager>().Play("Grab");
				books.SetActive(false);
				inventory.AddItem(items[2]);
				doctor.GetComponent<DoctorTrigger>().SetSpeakAble(false);
			}
			else
			{
				FindObjectOfType<DialogueManager>().StartDialogue(itemIntro);
			}
		}
		if (selected == placedObjects[12])
		{
			if (leader.GetComponent<LeaderTrigger>().GetIntroDone())
			{
				FindObjectOfType<AudioManager>().Play("Grab");
				inventory.AddItem(items[3]);
				medical.SetActive(false);
				doctor.GetComponent<DoctorTrigger>().SetSpeakAble(false);
			}
			else
			{
				FindObjectOfType<DialogueManager>().StartDialogue(itemIntro);
			}

		}
		if (selected == placedObjects[13])
		{
			if (leader.GetComponent<LeaderTrigger>().GetIntroDone())
			{
				FindObjectOfType<AudioManager>().Play("Grab");
				stetoscope.SetActive(false);
				inventory.AddItem(items[4]);
				doctor.GetComponent<DoctorTrigger>().SetSpeakAble(false);
			}
			else
			{
				FindObjectOfType<DialogueManager>().StartDialogue(itemIntro);
			}
		}
		if (selected == placedObjects[14])
		{
			if(duckDone){
			FindObjectOfType<AudioManager>().Play("HappyDuck");
			}
			else{
			FindObjectOfType<AudioManager>().Play("SickDuck");
			}
		}
		if (selected == placedObjects[15])
		{
			selected.GetComponent<DelivererTrigger>().TriggerDialogue();
		}
		if (selected == placedObjects[16])
		{
			FindObjectOfType<AudioManager>().Play("Car");
		}
		if (selected == placedObjects[17])
		{
			selected.GetComponent<Getter1Trigger>().TriggerDialogue();
		}
		if (selected == placedObjects[18])
		{
			selected.GetComponent<Getter2Trigger>().TriggerDialogue();
		}
		if (selected == placedObjects[19])
		{
			selected.GetComponent<Getter3Trigger>().TriggerDialogue();
		}
		if (selected == placedObjects[20])
		{
			selected.GetComponent<Getter4Trigger>().TriggerDialogue();
		}
		if (selected == placedObjects[21])
		{
			selected.GetComponent<Getter5Trigger>().TriggerDialogue();
		}
		if (selected == placedObjects[22])
		{
			selected.GetComponent<Doctor2Trigger>().EndDialogueLoop();
		}
		if (selected == placedObjects[23])
		{
			if(duckDone){
				FindObjectOfType<AudioManager>().Play("HappyDuck");
			}
			else{
				FindObjectOfType<AudioManager>().Play("SickDuck");
			}
		}
		if (selected == placedObjects[24])
		{
			if(duckDone){
				FindObjectOfType<AudioManager>().Play("HappyDuck");
			}
			else{
				FindObjectOfType<AudioManager>().Play("SickDuck");
			}
		}
    }

//das setzen der Berührungsrechte in der Updatefunktion. Wenn false, dann kann man kein virtuelles Objekt berühren. falls true, dann geht es
    public void SetTouch(bool touchable)
    {
        this.touchableAll = touchable;
    }
//fall beim rotfalken. wenn true, dann kann man erst ein Foto machen
    public void SetTouchable2(bool touchable2)
    {
        this.touchable2 = touchable2;
    }

//fall bei den Enten. wenn true gesetzt, dann sprechen die Enten normal. false sind sie krank.
	public void SetDuckDone(bool duckDone){
		this.duckDone=duckDone;

	}

}
