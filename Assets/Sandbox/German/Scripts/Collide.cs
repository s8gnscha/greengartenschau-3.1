﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/**
 * Wurde ursprünglich verwendet, um Objekte in der Nähe sichtbar zu machen. Bzw.
 * unsichtbar, wenn sie weit genug entfernt sind. Ist nicht mehr in Verwendung.
 */
public class Collide : MonoBehaviour
{


    public Text text;
    public GameObject object1;
    public GameObject object2;
    public GameObject object3;
    public GameObject object4;

    bool isTrigger;
    // Start is called before the first frame update
    void Start()
    {
        isTrigger = false;
    }

    // Update is called once per frame. Wenn istrigger an ist, dann sollen die objekte ersceinen
    void Update()
    {
        if (isTrigger)
        {
            object1.SetActive(true);
            object2.SetActive(true);
            object3.SetActive(true);
            object4.SetActive(true);
        }
        else
        {
            object1.SetActive(false);
            object2.SetActive(false);
            object3.SetActive(false);
            object4.SetActive(false);

        }
    }


    /**
     * Was passieren soll, wenn man im Trigger reinkommt. 
     */
    void OnTriggerEnter(Collider other)
    {
        text.text = "Done";
            isTrigger = true;
    }

    /**
     * was passieren soll, wenn man weiterhin drin ist
     */
    void OnTriggerStay(Collider other)
    {
        isTrigger = true;
    }

    /**
     * Was passieren soll, wenn man aus dem Trigger rausgeht
     */
    void OnTriggerExit(Collider other)
    {
        isTrigger = false;
    }


}
