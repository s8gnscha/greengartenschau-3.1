using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/**
 * Der zähler in der QuestUI
 */
public class SignatureCounter : MonoBehaviour
{

    public Dialogue done;
    private int counter;

    public Text signText;
    // Start is called before the first frame update
    void Start()
    {
        counter = 0;
        signText.text = ""+counter + "/3";
    }

    // Update is called once per frame. Stetige anzeige der Anzahl Petitionen
    void Update()
    {
        signText.text = ""+counter + "/3";
    }

    //Erhöht die Anzahl der Petition um 1. wenn anzahl gleich 3, dann ist das Spiel vorbei
    public void IncreaseCounter()
    {
        counter++;
        signText.text = ""+counter + "/3";

        if (counter >= 3)
        {
            FindObjectOfType<DialogueManager>().StartDialogue(done);
        }
    }
}
