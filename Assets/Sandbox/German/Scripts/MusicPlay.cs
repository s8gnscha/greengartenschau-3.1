﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * wurde verwendet, um musik jederzeit an und auszuschalten. ist nicht in Verwendung
 */
public class MusicPlay : MonoBehaviour
{

    bool play;
    // Start is called before the first frame update
    void Start()
    {
        play = true; 
    }

    // Update is called once per frame
    void Update()
    {
        if (play)
        {
           
        }
        else
        {
            
        }
    }

    /**
     * Funktion für Musik anmachen per Button. je nach knopfdruck geht er entweder an oder aus.
     */
    public void PlayMusic()
    {
        if (play)
        {
            FindObjectOfType<AudioManager>().Play("Music");
            play = false;
        }
        else
        {
            FindObjectOfType<AudioManager>().Stop("Music");
            play = true;
        }
    }
}
