﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Beschreibung für die Questobjekte
 */
[CreateAssetMenu(fileName = "New Item", menuName = "Quests/Quest")]
public class Quest: ScriptableObject
{
    public string title;
    
    [TextArea(3, 10)] 
    public string description;

    public bool done;
}
