﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Das Handeln mit den verschiedenen Funktionen
 */
public class Quests : MonoBehaviour
{
    public List<Quest> quests = new List<Quest>();


    #region Singleton

    public static Quests instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }

        instance = this;
    }

    #endregion


    public delegate void OnQuestChanged();
    public OnQuestChanged onQuestChangedCallback;

    public int space = 10;	// Amount of slots in inventory



    /**
     * Quest wird in die Missionsliste inzugefügt
     */
    public void AddQuest(Quest quest)
    {
        if (quests.Count >= space)
        {

            return;
        }
        quests.Add(quest);
        Debug.Log("picked up successful");
        if (onQuestChangedCallback != null)
            onQuestChangedCallback.Invoke();

    }

    /**
     * Quest in der Missionsliste wird verändert
     */
    public void ChangeQuest(Quest quest)
    {
        for (int i = 0; i < quests.Count; i++)
        {
            if (quests[i].title == quest.title)
            {
                quests[i] = quest;
            }
        }
    }


}
