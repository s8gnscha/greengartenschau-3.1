﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Das handeln der Missionsbeschreibung
 */
public class QuestDescription : MonoBehaviour
{

    public GameObject doneText;
    public GameObject questui;

    void Start()
    {
        Window.SetActive(false);
    }
    public Text title;
    public Text description;
    public GameObject Window;

    /**
     * funktion, welche genutzt wird, um die Missionsbeschreibung zu öffnen
     */
   public void OpenWindow(Quest quest)
    {
        Window.SetActive(true);
        title.text = quest.title;
        description.text = quest.description;
        questui.SetActive(false);
        if (quest.done)
        {
            doneText.SetActive(true);
        }
        else
        {
            doneText.SetActive(false);
        }
    }

    /**
 * Funktion, welche genutzt wird, um die Missionsbeschreibung zu schließen
 */
    public void CloseWindow()
    {
        Window.SetActive(false);
        questui.SetActive(true);
    }
}
