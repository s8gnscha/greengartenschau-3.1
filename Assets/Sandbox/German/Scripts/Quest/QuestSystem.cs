﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Handeln des allgemeinen Questsystems in der QuestUI
 */
public class QuestSystem : MonoBehaviour
{
    public Transform questsParent;
    public GameObject missionDescription;
    public GameObject questUI;
    Quests inventory;
    QuestSlot[] slots;


    public GameObject inventorybutton;
    public GameObject questbutton;
    public GameObject mission1;
    public GameObject intro;
    public GameObject arsession;

    // Start is called before the first frame update
    void Start()
    {
        questUI.SetActive(false);
        inventory = Quests.instance;
        inventory.onQuestChangedCallback += UpdateUI;
        slots = questsParent.GetComponentsInChildren<QuestSlot>();

    }

    /**
     * Updaten der allgemeinen Questslots in der QuestUI
     */
    void UpdateUI()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (i < inventory.quests.Count)  // If there is an item to add
            {
                slots[i].AddQuest(inventory.quests[i]);   // Add it
            }
            else
            {
                // Otherwise clear the slot
                slots[i].ClearSlot();
            }
        }
    }


    /**
     * Öffnen der Missionsbeschreibung
     */
    public void OpenDescription (int i)
    {
        missionDescription.GetComponent<QuestDescription>().OpenWindow(inventory.quests[i]);
    }

    /**
     * Öffnen der QuestUI
     */
    public void OpenWindow()
    {
        questUI.SetActive(true);
        inventorybutton.SetActive(false);
        questbutton.SetActive(false);
        arsession.GetComponent<SelectionController>().SetTouch(false);
        /*mission1.SetActive(false);
        intro.SetActive(false);*/
    }

    /**
     * Schließen der QuestUI
     */
    public void CloseWindow()
    {
        questUI.SetActive(false);
        inventorybutton.SetActive(true);
        questbutton.SetActive(true);
        arsession.GetComponent<SelectionController>().SetTouch(true);
        /*mission1.SetActive(true);
        intro.SetActive(true);*/
    }

}
