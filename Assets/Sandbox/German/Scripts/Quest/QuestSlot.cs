﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Das Handeln der einzelnen Questslots in der QuestUI
 */
public class QuestSlot : MonoBehaviour
{

    
    public Text title;
    Quest quest;

    /**
     * Hinzufügen einer Quest im Questslot
     */
    public void AddQuest(Quest newQuest)
    {
        quest = newQuest;
        title.text = newQuest.title;
        title.enabled = true;

 
    }

    /**
     * Entfernen einer Quest im Questslot
     */
    public void ClearSlot()
    {
        quest = null;
        title.text = null;
        title.enabled = false;
    }
}
