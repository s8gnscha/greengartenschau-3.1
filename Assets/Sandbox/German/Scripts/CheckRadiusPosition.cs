﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using ARLocation.UI;

/**
 * Die Datei ist dafür zuständig, damit die Fehlerrate im Mainmenu kalibriert wird
 */
public class CheckRadiusPosition : MonoBehaviour
{

    public GameObject gps;

    public float radius;

    public float gpslongitude;
    public float gpslatitude;

    public GameObject object1;
    public GameObject object2;
    public GameObject object3;
    public GameObject object4;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(GetAccess()); 
    }

/**
 * Kalibrierung der Fehlerrate, welche Input.location.lastData.horizontalAccuracy gibt
 */
    IEnumerator GetAccess()
    {
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
            yield break;

        // Start service before querying location
        Input.location.Start();

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            yield break;
        }
        else
        {

            CheckRadius();
            // Access granted and location value could be retrieved
            //print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
    }


    void CheckRadius()
    {

        //gps.GetComponent<PlaceAtLocation>().location.lastData.latitude
        /*float latitude = Input.location.lastData.latitude - gps.GetComponent<PlaceAtLocation>().Location.latitude;
        if (latitude < 0)
        {
            latitude = latitude * -1;
        }
        float longitude= Input.location.lastData.longitude - gps.GetComponent<PlaceAtLocation>().Location.longitude;
        if (longitude < 0)
        {
            longitude = longitude * -1;
        }

        if(longitude<radius && latitude < radius)
        {
            object1.SetActive(true);
            object2.SetActive(true);
            object3.SetActive(true);
            object4.SetActive(true);
        }
        else
        {
            object1.SetActive(false);
            object2.SetActive(false);
            object3.SetActive(false);
            object4.SetActive(false);
        }*/
    }

}
