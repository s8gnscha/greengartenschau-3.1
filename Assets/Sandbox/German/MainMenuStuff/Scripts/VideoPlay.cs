using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
public class VideoPlay : MonoBehaviour
{

    public VideoPlayer video;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Play()
    {
        video.GetComponent<VideoPlayer>().Play();
    }

    public void Pause()
    {
        video.GetComponent<VideoPlayer>().Pause();
    }
}
