using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuManager : MonoBehaviour
{

    public GameObject mainMenu;

    public GameObject calibrationMenu;

    public GameObject videoMenu;
    
    // Start is called before the first frame update
    void Start()
    {
        calibrationMenu.SetActive(false);
        videoMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToCalibrationMenu()
    {
        mainMenu.SetActive(false);
        calibrationMenu.SetActive(true);
        videoMenu.SetActive(false);
    }

    public void GoToVideoMenu()
    {
        mainMenu.SetActive(false);
        calibrationMenu.SetActive(false);
        videoMenu.SetActive(true); 
    }

    public void GoToGame()
    {
        SceneManager.LoadScene("Game");
    }
}
